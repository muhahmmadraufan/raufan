class Countrie < ActiveRecord::Base
  belongs_to :user
  validates :code => {:in => %w(id,usa,frc), :message => "%{value} is valid"}

  attr_accessible :code, :name
end
