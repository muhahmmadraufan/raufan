class User < ActiveRecord::Base
  has_many :countries, :dependent => :destroy
  has_many :articles, :dependent => :destroy
  has_many :products, :dependent => :destroy
  validates :first_name, :presence => true,
                         :length => {:minimum => 1, :maximum => 10},
                         :uniqueness => true,
                         :format => {:with => /[a-zA-Z\s]+/}
  validates :last_name, :presence => true,
                        :length => {:minimum => 1, :maximum => 10},
                        :uniqueness => true,
                        :format => {:with => /[a-zA-Z\s]+/}

  validates :email, :presence => true,
                    :uniqueness => true,
                    :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  def show_full_address
    "#{self.address}"
  end

   attr_accessible :first_name, :last_name, :email, :username, :address, :age, :birthday
end
