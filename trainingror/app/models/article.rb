class Article < ActiveRecord::Base
  has_many :comments, :dependent => :destroy

  belongs_to :user
  belongs_to :creator,
             :class_name => "User",
             :foreign_key => "user_id"

  validates :title, :presence => true, :inclusion => {:in => %w(nil,empty,blank), :message => "%{value} is not valid"},
                    :uniqueness => true

  scope :rate, lambda {|rating| where("rating > ?", rating)}
  def self.show_content
    where("body > 100")
  end
attr_accessible :title, :body, :rating
end
