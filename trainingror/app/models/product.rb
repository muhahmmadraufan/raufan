class Product < ActiveRecord::Base
  has_many :categories, :through => :product_categories
  has_many :product_categories

  belongs_to :user

  scope :greater, where("price > 1000")
  scope :word, where("name LIKE '%red%'")

  attr_accessible :description, :name, :price, :user_id, :category_id
end
