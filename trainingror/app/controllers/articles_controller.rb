class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def create
    @articles = Article.new
    render :action => :new
  end

  def new
    @article = Article.new
  end

  def welcome
    render :text => "Welcome to My Website"
  end
end
