class ChangeColumnFromTableCountrieAndTableArticle < ActiveRecord::Migration
  def up
  change_column :countries, :code, :string
  change_column :articles, :body, :text
  end

  def down
  change_column :countries, :code, :string
  change_column :articles, :body, :text
  end
end
