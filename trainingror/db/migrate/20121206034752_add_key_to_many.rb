class AddKeyToMany < ActiveRecord::Migration
  def change
    add_column :products, :category_id, :integer
    add_column :categories, :product_id, :integer
  end
end
