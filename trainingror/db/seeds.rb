# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Emanuel', :city => cities.first)

users = User.create([{ :first_name => "Muhammad", :last_name => "Raufan", :email => "raufan.yusup@wgs.co.id", :username => "muhammad.raufan" , :address => "gatot subroto", :age => "18", :birthday => "22-02-1994" }, { :first_name => "John", :last_name => "Martin", :email => "john@pragprog.com", :username => "john.martin" , :address => "LA", :age => "25", :birthday => "12-12-1987" }, { :first_name => "Leo", :last_name => "Messi", :email => "messi@test.com", :username => "leo.messi" , :address => "rosario", :age => "25", :birthday => "24-06-1987" }, { :first_name => "Martin", :last_name => "Seth", :email => "martin@testing.com", :username => "martin" , :address => "Roma", :age => "20", :birthday => "25-01-1992" }, { :first_name => "Tester", :last_name => "Testing", :email => "tester@testing.com", :username => "tester" , :address => "Paris", :age => "20", :birthday => "22-11-1992" }]) 

country = Countrie.create([{ :code => "62", :name => "Indonesia" }, { :code => "461", :name => "America" }, { :code => "188", :name => "Italy" }, { :code => "455", :name => "Spain" }, { :code => "235", :name => "France" }])

article = Article.create([{ :title => "news", :body => "lorem ipsum lorem ipsum lorem ipsum", :rating => "50" },  { :title => "newspaper", :body => "lorem ipsum lorem ipsum lorem ipsum", :rating => "40" }, { :title => "highlight", :body => "lorem ipsum lorem ipsum lorem ipsum", :rating => "60" }, { :title => "sport", :body => "lorem ipsum lorem ipsum lorem ipsum", :rating => "80" }, { :title => "reality", :body => "lorem ipsum lorem ipsum lorem ipsum", :rating => "40" }])

comment = Comment.create([{ :content => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum" }, { :content => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum" }, { :content => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum" }, { :content => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum" }, { :content => "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum" }])

product = Product.create([{ :name => "book of ruby", :price => "50000", :description => "tutorial of ruby", :category_id => "1", :user_id => "2" }])

category = Category.create([{ :name => "Book", :product_id => "1" }])
